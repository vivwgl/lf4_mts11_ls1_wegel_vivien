import java.util.Scanner;

class Fahrkartenautomat {
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		double rueckgabebetrag;
		double endBetrag;

		while (1 > 0) {
			// Fahrkartenbestellung
			endBetrag = fahrkartenbestellungErfassung(tastatur);

			// Geldeinwurf
			// -----------
			rueckgabebetrag = fahrkartenBezahlen(tastatur, endBetrag);

			// Fahrscheinausgabe
			// -----------------
			fahrkartenAusgeben();

			// R�ckgeldberechnung und -Ausgabe
			// -------------------------------

			rueckgeldAusgeben(rueckgabebetrag);
			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir w�nschen Ihnen eine gute Fahrt.\n\n");
		}

	}

	public static double fahrkartenbestellungErfassung(Scanner scan) {
		// System.out.print("Ticketpreis (EURO): ");
		String[] bezeichnung = { "Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC",
				"Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC",
				"Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC",
				"Kleingruppen-Tageskarte Berlin ABC" };
		double[] preis = { 2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90 };
		// Die Arrays k�nnen beliebig ver�ndert werden und die �nderungen sind
		// automatisch angewendet. Sie m�ssen somit nicht manuell im weiteren Code
		// angepasst werden. Sich ver�ndernde Preise oder Anzahl der angebotenen Tickets
		// ver�ndert sich auch mit. Die Implementierung neuer Tickets und �nderung schon
		// bestehender Tickets geht also schneller im Vergleich zu vorher
		double zuZahlenderBetrag = 0;
		System.out.println("Fahrkartenbestellvorgang:\n" + "=========================\n" + "\n"
				+ "W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:\n");
		for (int i = 0; i < bezeichnung.length; i++) {
			System.out.printf("%s [%.2f] \n", bezeichnung[i], preis[i]);
		}
		boolean found = false;
		while (found == false) {
			System.out.println("\nIhre Wahl: ");
			byte auswahlTicket = scan.nextByte();

			if (auswahlTicket < preis.length) {
				zuZahlenderBetrag = preis[auswahlTicket - 1];
				found = true;
			} else {
				System.out.println(">>falsche Eingabe<<");
			}
		}

		System.out.print("Anzahl der Tickets: ");
		byte anzahlTickets = scan.nextByte();
		double endBetrag = (double) (zuZahlenderBetrag * anzahlTickets);
		return endBetrag;

	}

	public static double fahrkartenBezahlen(Scanner scan, double endBetrag) {
		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneM�nze;
		while (eingezahlterGesamtbetrag < endBetrag) {
			System.out.printf("Noch zu zahlen: %.2f � \n", (endBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			eingeworfeneM�nze = scan.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}
		double r�ckgabebetrag = eingezahlterGesamtbetrag - endBetrag;
		return r�ckgabebetrag;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");

	}

	public static void rueckgeldAusgeben(double rueckgabebetrag) {

		if (rueckgabebetrag > 0.0) {
			System.out.printf("Der R�ckgabebetrag in H�he von %.2f � \n", rueckgabebetrag);
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (rueckgabebetrag >= 2.0) // 2 EURO-M�nzen
			{
				System.out.println("2 EURO");
				rueckgabebetrag -= 2.0;
			}
			while (rueckgabebetrag >= 1.0) // 1 EURO-M�nzen
			{
				System.out.println("1 EURO");
				rueckgabebetrag -= 1.0;
			}
			while (rueckgabebetrag >= 0.5) // 50 CENT-M�nzen
			{
				System.out.println("50 CENT");
				rueckgabebetrag -= 0.5;
			}
			while (rueckgabebetrag >= 0.2) // 20 CENT-M�nzen
			{
				System.out.println("20 CENT");
				rueckgabebetrag -= 0.2;
			}
			while (rueckgabebetrag >= 0.1) // 10 CENT-M�nzen
			{
				System.out.println("10 CENT");
				rueckgabebetrag -= 0.1;
			}
			while (rueckgabebetrag >= 0.05)// 5 CENT-M�nzen
			{
				System.out.println("5 CENT");
				rueckgabebetrag -= 0.05;
			}

		}
	}
}