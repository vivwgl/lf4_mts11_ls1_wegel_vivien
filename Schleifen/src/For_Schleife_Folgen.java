
public class For_Schleife_Folgen {
	public static void main(String[] args) {
		//a
		for (int i = 99; i >= 9; i -= 3) {
			System.out.println(i);
		}
		System.out.println("\n\n");
		//b
		int n = 0;
		for (int i = 1; i <= 400; i += (2*n+1)) {
			System.out.println(i);
			n++;
		}
		System.out.println("\n\n");

		//c
		for (int i = 2; i <= 102; i += 4) {
			System.out.println(i);
		}
		System.out.println("\n\n");
		
		//d
		for (int i = 2; i <= 32; i += 2) {
			System.out.println(i*i);
		}
		System.out.println("\n\n");
		
		//e
		for (int i = 2; i <= 32768; i = i*2) {
			System.out.println(i);
		}
		System.out.println("\n\n");
		
	}

}
