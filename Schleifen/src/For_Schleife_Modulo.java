
public class For_Schleife_Modulo {
	public static void main(String[] args) {
		System.out.println("Nummern die durch 7 teilbar sind: ");
		for(int i = 1; i <= 200; i++) {
			if (i % 7 == 0) {
			System.out.println(i);
			}
		}
		System.out.println("Alle Nummern die durch 4 aber nicht durch 5 teilbar sind: ");
		for(int i = 1; i <= 200; i++) {
			if (i % 4 == 0 && i % 5 != 0) {
			System.out.println(i);
			}
		}
	}

}
