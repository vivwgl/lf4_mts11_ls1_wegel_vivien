import java.util.Scanner;

public class For_Schleife_Summe {
	public static void main(String[] args) {
		//a
		Scanner scan = new Scanner(System.in);
		System.out.println("Geben Sie den Wert ein: ");
		int n = scan.nextInt();
		int summe = 0;
		for (int i = 1; i <= n; i++) {
				summe += i;
		}
		System.out.println("Die Summe f�r A betr�gt: " + summe);
		
		//b
		int endergebnis = 0;
		for (int i = 1; i <= n; i++) {
				endergebnis += 2*i;
		}
		System.out.println("Die Summe f�r B betr�gt: " + endergebnis);
		
		//c
		int ergebnis = 0;
		for (int i = 0; i < n; i++) {
				ergebnis += 2*i+1;
		}
		System.out.println("Die Summe f�r C betr�gt: " + ergebnis);
		
	}

}
