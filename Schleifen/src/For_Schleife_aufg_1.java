import java.util.Scanner;

public class For_Schleife_aufg_1 {
	public static void main(String[] args) {
		//a)
		Scanner scan = new Scanner(System.in);
		System.out.println("Geben Sie eine Zahl ein: ");
		int n = scan.nextInt();
		for (int i = 1; i <= n; i++)
				System.out.println(i);
		System.out.println("\n\n");
		//b 
		for(int i = n; i > 0; i--)
			System.out.println(i);
		
	}

}
