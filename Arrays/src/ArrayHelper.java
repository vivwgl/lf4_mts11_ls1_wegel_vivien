
public class ArrayHelper {
	public static void main(String[] args) {
		
		int[] zahlen = {2, 6, 3, 1, 8};
		System.out.println(convertArrayToString(zahlen));
		
	}
	public static String convertArrayToString(int[] zahlen) {
		String numbers = "";
		for(int i = 0; i < zahlen.length; i ++) {
			if (i == zahlen.length -1) {
				numbers = numbers + zahlen[i];
			}
			else {
				numbers = numbers + zahlen[i] + ", ";
			}
			
		}
		return numbers;
	}

}
