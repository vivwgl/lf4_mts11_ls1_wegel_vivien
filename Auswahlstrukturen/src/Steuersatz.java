import java.util.Scanner;

public class Steuersatz {
	public static void main(String[] args) {
		System.out.println("Bitte geben Sie den Nettowert ein: ");
		Scanner scan = new Scanner(System.in); 
		double nettowert = scan.nextDouble();
		char antwort = abfrage(scan);
		System.out.println(berechnungBruttobetrag(antwort, nettowert));
		
		
	}
	public static char abfrage(Scanner scan) {
		System.out.println("Soll der volle oder der gem��igte Steuersatz angewendet werden? F�r den vollen Steursatz geben Sie \"j\" ein, f�r den erm��igten Steuersatz \"n\".");
		char antwort = scan.next().charAt(0);
		return antwort;
	}
	public static double berechnungBruttobetrag(char antwort, double nettowert) {
		double result = 0.0;
		if (antwort == 'j') {
			result = nettowert * 1.07;
		}
		else if(antwort == 'n'){
			result = nettowert * 1.19;
		}
		
		return result;
	} 

}
