
public class EigeneBedingungen {

	public static void main(String[] args) {
		// Aufgabe 1.1.2
		int x = 1;
		int y = 1;
		if (x == y) {
			System.out.println("x und y haben den selben Wert.");
		}
		
		// Aufgabe 1.1.3
		int a = 2;
		int b = 0;
		int c = 1;
		if(b<a) {
			System.out.println("Die zweite Zahl ist gr��er als die erste Zahl.");
		}
		//Aufgabe 1.1.4
		if(a>=b) {
			System.out.println("Die erste Zahl ist gr��er oder gleich der zweiten Zahl.");
		}
		else {
			System.out.println("Die zweite Zahl ist gr��er als die erste Zahl.");
		}
		
		
		// Aufgabe 1.2.1
		if (a>b && a>c) {
			System.out.println("Die erste Zahl ist die gr��te Zahl.");
		}
		
		// Aufgabe 1.2.2
		if (c>a || a>b) {
			System.out.println("Die dritte Zahl ist gr��er als die erste oder die zweite Zahl.");
		}
		
		// Aufgabe 1.2.3
		if(a>b && a>c) {
			System.out.println("Die erste Zahl ist die gr��te Zahl: " + a);
		}
		else if(b>a && b>c) {
			System.out.println("Die zweite Zahl ist die gr��te Zahl: " + b);
		}
		else {
			System.out.println("Die dritte Zahl ist die gr��te Zahl: " + c);
		}
	}

}
