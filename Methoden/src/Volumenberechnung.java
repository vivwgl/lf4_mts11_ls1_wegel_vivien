
public class Volumenberechnung {

	public static void main(String[] args) {
		double a = 2.34;
		double b = 3.12;
		double c = 4.25;
		double h = 5.21;
		double r = 1.64;
		
		System.out.println(wuerfel(a));	
		System.out.println(quader(a, b, c));	
		System.out.println(pyramide(a, h));	
		System.out.println(kugel(r));	

	}
	public static double wuerfel(double wert) {
		double ergebnis = wert * wert * wert;
		return ergebnis;
	}
	public static double quader(double wert1, double wert2, double wert3) {
		double ergebnis = wert1 * wert2 * wert3;
		return ergebnis;
	}
	public static double pyramide(double wert, double hoehe) {
		double ergebnis = wert * wert * hoehe/3;
		return ergebnis;
	}
	public static double kugel(double radius) {
		double ergebnis =  4/3 * (radius * radius * radius) * Math.PI;
		return ergebnis;
	}

}
